import 'package:dio/dio.dart';
import 'package:flutter_retrofit_sample/repository/retrofit/api_client.dart';

class PAPIRepo {
  PAPIClient _apiRequest;
  Dio dio;

  PAPIRepo() {
    dio = Dio();
    _apiRequest = PAPIClient(dio);
  }
}
