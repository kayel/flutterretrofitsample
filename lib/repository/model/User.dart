class User {
  int id;
  String name;
  String email;
  String gender;
  String status;
  String created;
  String updated;

  User({this.id, this.name, this.email, this.gender, this.status, this.created, this.updated});

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        gender: json['gender'],
        status: json['status'],
        created: json['created_at'],
        updated: json['updated_at'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'gender': gender,
        'status': status,
        'created_at': created,
        'updated_at': updated,
      };
}
