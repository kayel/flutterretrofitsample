// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as int,
    name: json['name'] as String,
    email: json['email'] as String,
    gender: json['gender'] as String,
    status: json['status'] as String,
    created: json['created_at'] as String,
    updated: json['updated_at'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'gender': instance.gender,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updated,
    };

ResponseUsers _$ResponseDataFromJson(Map<String, dynamic> json) {
  return ResponseUsers(
    httpCode: json['code'] as int,
    meta: json['meta'],
    users: json['data'] as List,
  );
}

Map<String, dynamic> _$ResponseDataToJson(ResponseUsers instance) =>
    <String, dynamic>{
      'code': instance.httpCode,
      'meta': instance.meta,
      'data': instance.users,
    };
