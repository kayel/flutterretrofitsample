import 'package:json_annotation/json_annotation.dart';

part 'data.g.dart';

@JsonSerializable()
class User {
  int id;
  String name;
  String email;
  String gender;
  String status;
  String created;
  String updated;

  User(
      {this.id,
      this.name,
      this.email,
      this.gender,
      this.status,
      this.created,
      this.updated});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class ResponseUsers {
  int httpCode;
  dynamic meta;
  List<dynamic> users;

  ResponseUsers({this.httpCode, this.meta, this.users});

  factory ResponseUsers.fromJson(Map<String, dynamic> json) =>
      _$ResponseDataFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseDataToJson(this);
}
