import 'package:dio/dio.dart';
import 'package:flutter_retrofit_sample/repository/model/data.dart';
import 'package:retrofit/http.dart';
import 'public_api.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: PAPI.base)
abstract class PAPIClient {
  factory PAPIClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET(PAPI.users)
  Future<ResponseUsers> getUsers();
}
