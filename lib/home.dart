import 'package:flutter/cupertino.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_retrofit_sample/repository/model/data.dart';
import 'package:flutter_retrofit_sample/repository/retrofit/api_client.dart';

PAPIClient apiClient = PAPIClient(Dio(BaseOptions(contentType: "application/json")));

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.teal[50],
        appBar: appBar(),
        body: body(context)
  );

  AppBar appBar() => AppBar(
      title: Text("Flutter Retrofit Sample", style: TextStyle(color: Colors.grey[900])),
      backgroundColor: Colors.teal[100]);

  FutureBuilder<ResponseUsers> body(BuildContext context) => FutureBuilder<ResponseUsers>(
        future: apiClient.getUsers(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            return buildListView(context, snapshot.data);
          else
            return Center(child: CircularProgressIndicator());
        },
      );

  Widget buildListView(BuildContext context, ResponseUsers responseUsers) => ListView.builder(
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.only(left: 8.0, top: 4.0, right: 8.0, bottom: 4.0),
          child: Card(
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: ListTile(
                    leading: Icon(Icons.contact_mail_outlined, color: Colors.grey[700], size: 48),
                    title: Text(responseUsers.users[index]['name'], style: TextStyle(fontSize: 20)),
                    subtitle: Text(responseUsers.users[index]['email']),
                onTap: () {

                }),
              )),
        ),
        itemCount: responseUsers.users.length,
      );
}
