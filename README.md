# Flutter Retrofit Sample
[Download DEBUG APK](https://bitbucket.org/kayel/flutterretrofitsample/src/master/build/app/outputs/apk/debug/app-universal-debug.apk)

# Dependencies

 - retrofit: ^1.3.4+1
 - dio: ^3.0.10
 - built_value: ^7.1.0
 - json_annotation: ^3.1.0